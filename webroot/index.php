<?php

$filename = "content/content.txt";
$file = fopen ($filename, "r");
$lineCount = 0;
while (!feof ($file)) {
  $nullobject = fgets ($file);
  $lineCount++;
}
fclose ($file);
function displayContent () {
  global $filename;
  $file = fopen ($filename, "r");
  $i = 0;
  while (!feof ($file)) {
    if ($i != 0) {
      echo '      ';
    }
    $lineNumber = $i + 1;
    echo '<div class="contentLine" id="' . $lineNumber . '">' . trim (fgets ($file), "\n") . '</div>' . "\n";
    $i++;
  }
  fclose ($file);
}

?>

<html>
  <head>
    <style>
      #body {
        background-color: #000000;
        color: #00FFFF;
        font-family: Arial;
        font-weight: bold;
        cursor: default;
        overflow: hidden;
        transform: scaleX(1);
        margin: 0px;
      }
      #header {
        position: absolute;
        background-color: #000000;
        top: 0px;
        width: 100%;
        padding-top: 20px;
        height: 45px;
        z-index: 1;
      }
      #content {
        position: relative;
        background-color: #000000;
        padding-top: 265px;
        padding-left: 40px;
        padding-right: 40px;
        top: 0px;
      }
      #topBlock {
        position: absolute;
        top: 65px;
        height: 120px;
      }
      #bottomBlock {
        position: fixed;
        top: 305px;
        height: 100%;
      }
      .filterBlock {
        background-color: #000044;
        opacity: 0.8;
        width: 100%;
      }
      .contentLine {
        position: relative;
        line-height: 40px;
        font-size: 24pt;
      }
      .btnMarginLeft {
        margin-left: 40px;
      }
      .button {
        position: relative;
        top: 0px;
        text-align: center;
        display: inline-block;
        width: 24px;
        height: 24px;
      }
      .buttonLabel {
        display: inline-block;
        font-size: 18pt;
        text-align: center;
        padding-left: 10px;
        padding-right: 10px;
      }
    </style>
    <script>
      var pos = 0;
      var pauseScroll = false;
      var frameInterval = 120;
      function scrollPlay () {
        var contentDiv = document.getElementById ("content");
        pauseScroll = false;
        var bodyDiv = document.getElementById ("body");
        var contentHeight = contentDiv.offsetHeight;
        var bodyHeight = bodyDiv.clientHeight;
        var pixelInterval = 1;
        var maxPos = (0 - bodyHeight - contentHeight);
        var taskInterval = setTimeout (function animateScroll () {
          if (pos > maxPos) {
            pos -= pixelInterval;
            contentDiv.style.top = pos + "px";
            if (pauseScroll == false) {
              taskInterval = setTimeout (animateScroll, frameInterval);
            }
          }
        }, frameInterval);
      }
      function scrollPause () {
        pauseScroll = true;
      }
      function decreaseSpeed () {
        frameInterval += 20;
      }
      function increaseSpeed () {
        frameInterval -= 20;
      }
      function jumpUp () {
        var contentDiv = document.getElementById ("content");
        pos = Math.floor (pos / 40) * 40 + 40;
        contentDiv.style.top = pos + "px";
      }
      function jumpDn () {
        var contentDiv = document.getElementById ("content");
        pos = Math.ceil (pos / 40) * 40 - 40;
        contentDiv.style.top = pos + "px";
      }
      function resetScroll () {
        var contentDiv = document.getElementById ("content");
        if (pauseScroll == false) {
          scrollPause ();
        }
        pos = 0;
        contentDiv.style.top = pos + "px";
      }
      function horizFlip () {
        var bodyTransformState = document.getElementById ("body").style.transform;
        if (bodyTransformState == "scaleX(-1)") {
          document.getElementById ("body").style.transform = "scaleX(1)";
        } else {
          document.getElementById ("body").style.transform = "scaleX(-1)";
        }
      }
    </script>
  </head>
  <body id="body">
    <div id="header">
      <div id="btnPlay" class="button btnMarginLeft" onclick="scrollPlay ()"><img src="images/Play.svg" /></div>
      <div id="btnPause" class="button btnMarginLeft" onclick="scrollPause ()"><img src="images/Pause.svg" /></div>
      <div id="btnSpeedDec" class="button btnMarginLeft" onclick="decreaseSpeed ()"><img src="images/Minus.svg" /></div>
      <div class="buttonLabel">Speed</div>
      <div id="btnSpeedInc" class="button" onclick="increaseSpeed ()"><img src="images/Plus.svg" /></div>
      <div id="btnJumpUp" class="button btnMarginLeft" onclick="jumpUp ()"><img src="images/Up.svg" /></div>
      <div class="buttonLabel">Jump</div>
      <div id="btnJumpDn" class="button" onclick="jumpDn ()"><img src="images/Down.svg" /></div>
      <div id="btnReset" class="button btnMarginLeft" onclick="resetScroll ()"><img src="images/Reset.svg" /></div>
      <div id="btnHorizFlip" class="button btnMarginLeft" onclick="horizFlip ()"><img src="images/FlipHoriz.svg" /></div>
    </div>
    <div id="content">
      <?php displayContent (); ?>
    </div>
    <div id="topBlock" class="filterBlock"></div>
    <div id="middleblock"></div>
    <div id="bottomBlock" class="filterBlock"></div>
  </body>
</html>
